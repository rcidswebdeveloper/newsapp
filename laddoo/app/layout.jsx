import Header from './Header';
import Footer from './Footer';
import Hero from './Hero';
import 'tailwindcss/tailwind.css'

export default function Layout({ children }) {
  return (
    <div>
      <Header />
      <main>{children}</main>
      <Hero />
      
    </div>
  );
}
