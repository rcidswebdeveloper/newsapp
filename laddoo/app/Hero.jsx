import Link from 'next/link';
import styles from './globals.css';



export default function Header() {
  return (
    <div>
      <div className="py-6">
        <h3 style={{ fontWeight: 'bold' }} className="text-2xl mb-4">Trending Topics</h3>

        <div className="carousel w-full">
          <div id="slide1" className="carousel-item relative w-full">
            <img src="https://images.unsplash.com/photo-1557992260-ec58e38d363c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80" className="w-full" />
            <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
              <a href="#slide4" className="btn btn-circle"></a> 
              <a href="#slide2" className="btn btn-circle"></a>
            </div>
          </div> 
          <div id="slide2" className="carousel-item relative w-full">
            <img src="/images/stock/photo-1609621838510-5ad474b7d25d.jpg" className="w-full" />
            <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
              <a href="#slide1" className="btn btn-circle"></a> 
              <a href="#slide3" className="btn btn-circle"></a>
            </div>
          </div> 
          <div id="slide3" className="carousel-item relative w-full">
            <img src="/images/stock/photo-1414694762283-acccc27bca85.jpg" className="w-full" />
            <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
              <a href="#slide2" className="btn btn-circle"></a> 
              <a href="#slide4" className="btn btn-circle"></a>
            </div>
          </div> 
          <div id="slide4" className="carousel-item relative w-full">
            <img src="/images/stock/photo-1665553365602-b2fb8e5d1707.jpg" className="w-full" />
            <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">

              <a href="#slide3" className="btn btn-circle"></a> 
              <a href="#slide1" className="btn btn-circle"></a>
            </div>
          </div>
        </div>
      </div>

      <>
      <header>
        <nav >
          <ul>
            <li>
              <Link href="/">
                <span>Today</span>
              </Link>
            </li>
            <li>
              <Link href="/entertainment">
                <span>Sports</span>
              </Link>
            </li>
            <li>
              <Link href="/sports">
                <span>Health</span>
              </Link>
            </li>
            <li>
              <Link href="/health">
                <span>Entertainment</span>
              </Link>
            </li>
            <li>
              <Link href="/crime">
                <span>Crime</span>
              </Link>
            </li>
            <li>
              <Link href="/tech">
                <span>Technology</span>
              </Link>
            </li>
            <li>
              <Link href="/lifestyle">
                <span>Lifestyle</span>
              </Link>
            </li>
            <li>
              <Link href="/weather">
                <span>Weather</span>
              </Link>
            </li>
          </ul>
        </nav>
      </header>

      <div className="card lg:card-side bg-base-100 shadow-xl">
        <figure><img src="/images/stock/photo-1494232410401-ad00d5433cfa.jpg" alt="Album"/></figure>
        <div className="card-body">
          <h2 className="card-title">New News is released!</h2>
          <p>Click the button to listen on Spotiwhy app.</p>
          <div className="card-actions justify-end">
            <button className="btn btn-primary">More</button>
          </div>
        </div>
      </div>
      <div className="card lg:card-side bg-base-100 shadow-xl">
        <figure><img src="/images/stock/photo-1494232410401-ad00d5433cfa.jpg" alt="Album"/></figure>
        <div className="card-body">
          <h2 className="card-title">New News is released!</h2>
          <p>Click the button to listen on Spotiwhy app.</p>
          <div className="card-actions justify-end">
            <button className="btn btn-primary">More</button>
          </div>
        </div>
      </div>
      <div className="card lg:card-side bg-base-100 shadow-xl">
        <figure><img src="/images/stock/photo-1494232410401-ad00d5433cfa.jpg" alt="Album"/></figure>
        <div className="card-body">
          <h2 className="card-title">New album is released!</h2>
          <p>Click the button to listen on Spotiwhy app.</p>
          <div className="card-actions justify-end">
            <button className="btn btn-primary">More</button>
          </div>
        </div>
      </div>
    </>
    </div>
  );
}